package com.hibernateDemo;


import com.hibernateDemo.vo.Student;
import com.hibernateDemo.vo.StudentDetail;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class HibernateDemo {


    public static void main(String[] args) {
        final SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(StudentDetail.class)
                .buildSessionFactory();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try (Session session = sessionFactory.getCurrentSession()) {
                    Thread.sleep(2000);
                    session.beginTransaction();
                    Student student = session.get(Student.class, 4);
                    System.out.println(student);
                    session.createQuery("UPDATE Student s set s.firstName=:firstName where s.id=4").setParameter("firstName", "Jhon").executeUpdate();
//            Student student = new Student("Murali", "Manohar", "muralimanohar707@gmail.com", new StudentDetail("youtube.com/Murali", "29/Sep/1997"));
//            session.save(student);
//            StudentDetail detail = session.get(StudentDetail.class, 3);
//            System.out.println(detail);
//            detail.getStudent().setStudentDetail(null);
//            session.delete(detail);
                    session.getTransaction().commit();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    sessionFactory.close();
                }
            }
        });
        thread.start();
        System.out.println("Executing... This");


    }


}
