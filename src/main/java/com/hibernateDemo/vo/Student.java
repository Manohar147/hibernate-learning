package com.hibernateDemo.vo;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "student_table")
@Getter
@Setter
@ToString(exclude = "studentDetail")
@NoArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_detail_id")
    private StudentDetail studentDetail;

    public Student(String firstName, String lastName, String email, StudentDetail studentDetail) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.studentDetail = studentDetail;
    }




}
