package com.hibernateDemo.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "student_detail")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class StudentDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "youtube_channel")
    private String youtubeChannel;

    @Column(name = "dob")
    private String dob;

//    @OneToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, mappedBy = "studentDetail",targetEntity = Student.class)
//    private Student student;

    public StudentDetail(String youtubeChannel, String dob) {
        this.youtubeChannel = youtubeChannel;
        this.dob = dob;
    }

}
